import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

/**
 * RSA (két) kulcsos kódolás - dekódolás (titkosítás - feloldás) használata.
 * Megadott kulcsok kezelése - beolvasása állományból, tömbből.
 * Megadott file-ok - állományrendszerbő és tömbből - (ki|be)kódolása.
 *
 * @author Varsányi Péter
 */
public class AshAssymetricCryptography {
    private Cipher cipher;

    public AshAssymetricCryptography() throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.cipher = Cipher.getInstance("RSA");
    }

    /**
     * Privát kulcs beolvasása file-ból.
     *
     * @param targetFile A file neve teljes elérési úttal.
     * @return A privát kulcs.
     * @throws Exception Bármi hiba volt, akkor exception.
     */
    public PrivateKey getPrivate( String targetFile ) throws Exception {
        return getPrivate(Files.readAllBytes(new File(targetFile).toPath()));
    }

    /**
     * Privát kulcs előállítása adattömbből.
     * https://docs.oracle.com/javase/8/docs/api/java/security/spec/PKCS8EncodedKeySpec.html
     *
     * @param keyBytes A kulcs adattömb.
     * @return A privát kulcs.
     * @throws Exception Bármi hiba volt, akkor exception.
     */
    public PrivateKey getPrivate( byte[] keyBytes ) throws Exception {
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    /**
     * Publikus kulcs beolvasása file-ból.
     *
     * @param targetFile A file neve teljes elérési úttal.
     * @return A publikus kulcs.
     * @throws Exception Bármi hiba volt, akkor exception.
     */
    public PublicKey getPublic( String targetFile ) throws Exception {
        return getPublic(Files.readAllBytes(new File(targetFile).toPath()));
    }

    /**
     * Publikus kulcs beolvasása file-ból.
     * https://docs.oracle.com/javase/8/docs/api/java/security/spec/X509EncodedKeySpec.html
     *
     * @param keyBytes A kulcs adattömb.
     * @return A publikus kulcs.
     * @throws Exception Bármi hiba volt, akkor exception.
     */
    public PublicKey getPublic( byte[] keyBytes ) throws Exception {
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    /**
     * Megadott tartalom fizikai (file rendszeri) állományba kódolása (titkosítása).
     *
     * @param content    A kódlandó tartalom.
     * @param targetFile A kimenti állomány neve elérési úttal.
     * @param key        A kódoláshoz használt kulcs.
     * @throws IOException              Ha bámilyen állomány kezelési hiba volt.
     * @throws GeneralSecurityException Egyéb kódolási hiba.
     */
    public void encryptToFile( byte[] content, File targetFile, Key key )
            throws IOException, GeneralSecurityException {
        writeToFile(targetFile, encrypt(content, key));
    }

    /**
     * Megadott tartalom fizikai (file rendszeri) állományba kikódolása (titkosítás feloldása).
     *
     * @param content    A titkosítás alól felodandó adattartalom.
     * @param targetFile A kimenti állomány neve elérési úttal.
     * @param key        A feloldáshoz használt kulcs.
     * @throws IOException              Ha bámilyen állomány kezelési hiba volt.
     * @throws GeneralSecurityException Egyéb kódolási hiba.
     */
    public void decryptToFile( byte[] content, File targetFile, Key key )
            throws IOException, GeneralSecurityException {
        writeToFile(targetFile, decrypt(content, key));
    }

    /**
     * Adattartalom titkosítása.
     *
     * @param content A titkosítandó adattartalom.
     * @param key     A titkosításhoz szükséges kulcs.
     * @return A titkosított adattartalom.
     * @throws GeneralSecurityException Bármi hiba kivétlként jelenik meg.
     */
    public byte[] encrypt( byte[] content, Key key )
            throws GeneralSecurityException {
        this.cipher.init(Cipher.ENCRYPT_MODE, key);
        return cryptData(content, 117);
    }

    /**
     * Titkosított adattartalom feloldása.
     *
     * @param content A titkosított adattartalom.
     * @param key     A feloldáshoz szükséges kulcs.
     * @return A feloldott adattartalom.
     * @throws GeneralSecurityException Bármi hiba kivétlként jelenik meg.
     */
    public byte[] decrypt( byte[] content, Key key )
            throws GeneralSecurityException {
        this.cipher.init(Cipher.DECRYPT_MODE, key);
        return cryptData(content, 128);
    }

    /**
     * A megadott adattartalom titkosítása vagy feloldása - a beállított <code>chiper</code> fieldtől függően.
     * Ha túl nagy az adattartalom (ez a kulcs hosszától függ), akkor blokkonként kódolja.
     *
     * @param content        A forrás adattartalom.
     * @param maxBlockLength A feldolgozható maximális block mérete. Ezt a kódolási kulcs mérete adja meg, és változó attól függően mekkora kulcsméretet használ a kódolás..
     * @return A módosított adattartalom.
     * @throws BadPaddingException       Bármi hiba kivétlként jelenik meg.
     * @throws IllegalBlockSizeException Bármi hiba kivétlként jelenik meg.
     */
    private byte[] cryptData( byte[] content, int maxBlockLength ) throws BadPaddingException, IllegalBlockSizeException {
        byte[] retVal = new byte[0];
        int readFromIdx = 0;
        while ( readFromIdx < content.length ) {
            int readToIdx = readFromIdx + maxBlockLength > content.length ? content.length : readFromIdx + maxBlockLength;
            byte[] sourceBlock = Arrays.copyOfRange(content, readFromIdx, readToIdx);

            byte[] resultBlock = this.cipher.doFinal(sourceBlock);

            byte[] newRetVal = new byte[retVal.length + resultBlock.length];
            System.arraycopy(retVal, 0, newRetVal, 0, retVal.length);
            System.arraycopy(resultBlock, 0, newRetVal, retVal.length, resultBlock.length);
            retVal = newRetVal;

            readFromIdx = readToIdx;
        }
        return retVal;
    }

    /**
     * A megadott adattartalom állományba írása.
     *
     * @param targetFile A kimeneti állomány neve elérési úttal.
     * @param content    A kiírandó adattartalom.
     * @throws IllegalBlockSizeException Ha bámilyen állomány kezelési hiba volt.
     * @throws BadPaddingException       Ha bámilyen állomány kezelési hiba volt.
     * @throws IOException               Ha bámilyen állomány kezelési hiba volt.
     */
    private void writeToFile( File targetFile, byte[] content )
            throws IllegalBlockSizeException, BadPaddingException, IOException {
        FileOutputStream fos = new FileOutputStream(targetFile);
        fos.write(content);
        fos.flush();
        fos.close();
    }

    /**
     * Megadott szöveges (<code>String</code>) üzenet titkosítása.
     *
     * @param msg A titkosítandó szöveg.
     * @param key A kulcs.
     * @return A titkosított szöveg.
     * @throws GeneralSecurityException     Bármi hiba kivétlként jelenik meg.
     * @throws UnsupportedEncodingException Bármi hiba kivétlként jelenik meg.
     */
    public String encryptText( String msg, Key key )
            throws GeneralSecurityException, UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(encrypt(msg.getBytes("UTF-8"), key));
    }

    /**
     * Megadott titkosított szöveg (<code>String</code>) feloldása.
     *
     * @param msg A titkosított szöveg.
     * @param key A kulcs.
     * @return A titkosítás alól feloldott szöveg.
     * @throws GeneralSecurityException     Bármi hiba kivétlként jelenik meg.
     * @throws UnsupportedEncodingException Bármi hiba kivétlként jelenik meg.
     */
    public String decryptText( String msg, Key key )
            throws GeneralSecurityException, UnsupportedEncodingException {
        return new String(decrypt(Base64.getDecoder().decode(msg), key), "UTF-8");
    }

    /**
     * Csak a teszteléshez kell.
     *
     * @return A kódoláshoz használt chiper mező.
     */
    public Cipher getCipher() {
        return cipher;
    }
}
