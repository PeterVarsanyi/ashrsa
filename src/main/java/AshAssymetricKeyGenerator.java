import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;

/**
 * A kulcspár generálását végző osztály.
 * A példányosítás során inicializálódik az algoritmus generátor. A kulcs hosszát kell megadni paraméterként.
 * RSA algoritmust használ.
 * <p>
 * A létrehozott kulcsokat ki tudja írni állományba (<code>writeToFile</code> method).
 *
 * @author Varsányi Péter
 */
public class AshAssymetricKeyGenerator {

    private KEYSIZE keysize;
    private KeyPairGenerator keyGen;
    private PrivateKey privateKey;
    private PublicKey publicKey;

    /**
     * A kulcspár generálásának indítása.
     * RSA algoritmust használ.
     * Az algoritmus inicializálása.
     *
     * @param keysize A kulcs hossza.
     * @throws NoSuchAlgorithmException Nincs meg a megadott algoritums.
     * @throws NoSuchProviderException  Nincs provider.
     */
    public AshAssymetricKeyGenerator(KEYSIZE keysize) throws NoSuchAlgorithmException, NoSuchProviderException {
        this.keysize = keysize;
        this.keyGen = KeyPairGenerator.getInstance("RSA");
        this.keyGen.initialize(keysize.keySize);
    }

    /**
     * Új kulcspár létrehozása.
     */
    public void createKeys() {
        KeyPair pair = this.keyGen.generateKeyPair();
        this.privateKey = pair.getPrivate();
        this.publicKey = pair.getPublic();
    }

    /**
     * A privát kulcs.
     *
     * @return A privát kulcs objektum.
     */
    public PrivateKey getPrivateKey() {
        return this.privateKey;
    }

    /**
     * A publkus kulcs.
     *
     * @return A publikus kulcs objektum.
     */
    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    /**
     * A megadott kulcs állományba írása.
     * Ha nincs meg a <code>path</code> paraméterben lévő útvonal, létrehozza.
     *
     * @param path A létrehoznadó kulcs állomány elérési újta, névvel.
     * @param key  A kulcs értéke <code>byte</code> tömbként.
     * @throws IOException Ha az aállomány írása közben történt valami hiba.
     */
    public void writeToFile( String path, byte[] key ) throws IOException {
        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(key);
        fos.flush();
        fos.close();
    }

    /**
     * A generálás során használ kulcs mérete.
     *
     * @return Kulcs mérete saját enum.
     */
    public KEYSIZE getKeysize() {
        return keysize;
    }

    public enum KEYSIZE {

        KEY1024(1024, 117, 128);

        private int keySize;
        private int decryptMaxBlockLength;
        private int encryptMaxBlockLength;

        KEYSIZE( int keySize, int decryptMaxBlockLength, int encryptMaxBlockLength ) {
            this.keySize = keySize;
            this.decryptMaxBlockLength = decryptMaxBlockLength;
            this.encryptMaxBlockLength = encryptMaxBlockLength;
        }
    }
}
