import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

/**
 * Aláírás és aláírás ellenőrése.
 * Két kulcsos.
 *
 * @author Varsányi Péter
 */
public class AshSigning {

    // alírás/ellenőrzés algoritmus
    private String signatureAlgorithm = "SHA1WithRSA";

    /**
     * Aláírás/ellenőrzés.
     * Default algorithm: SHA1WithRSA
     */
    public AshSigning() {
    }

    /**
     * Alírás/ellenőrzés a megadott algoritmus használatával.
     *
     * @param signatureAlgorithm String - az algoritmus azonosítója, pl.: SHA1WithRSA.
     */
    public AshSigning(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    /**
     * Byte adattartalom alapján aláírás képzése privát kulccsal, az eredmény <code>String</code>.
     *
     * @param data       Az aláirandó adattartalom.
     * @param privateKey Privát kulcs.
     * @return Az aláírás stringként, Base64 kódolással.
     * @throws GeneralSecurityException
     */
    public String createStrSignature(byte[] data, PrivateKey privateKey) throws GeneralSecurityException {
        return java.util.Base64.getEncoder().encodeToString(this.createSignature(data, privateKey));
    }

    /**
     * Byte adattartalom alapján aláírás képzése privát kulccsal, az eredmény <code>byte</code> tömb.
     *
     * @param data       Az aláirandó adattartalom.
     * @param privateKey Privát kulcs.
     * @return Az aláírás byte tömbben.
     * @throws GeneralSecurityException
     */
    public byte[] createSignature(byte[] data, PrivateKey privateKey) throws GeneralSecurityException {
        Signature sig = Signature.getInstance(signatureAlgorithm);
        sig.initSign(privateKey);
        sig.update(data);
        return sig.sign();
    }

    /**
     * Aláírt byte adattartalom ellenőrzése publikus kulccsal.
     *
     * @param data      Az ellenőrzésre szát adattartalom.
     * @param signature Az aláírt adattartalom.
     * @param publicKey A publikus kulcs.
     * @return Az ellenőrzés eredménye.
     * @throws GeneralSecurityException
     */
    public Boolean verify(byte[] data, byte[] signature, PublicKey publicKey) throws GeneralSecurityException {
        Signature sig = Signature.getInstance(signatureAlgorithm);
        sig.initVerify(publicKey);
        sig.update(data);
        return sig.verify(signature);
    }

    /**
     * Az aláírás/ellenőrzés algoritmus.
     *
     * @return Az aláírás/ellenőrzés algoritmus, pl.: SHA1WithRSA.
     */
    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    /**
     * Az aláírás/ellenőrzés algoritmus beállítása.
     *
     * @param signatureAlgorithm Az aláírás/ellenőrzés algoritmus, pl.: SHA1WithRSA.
     */
    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }
}
