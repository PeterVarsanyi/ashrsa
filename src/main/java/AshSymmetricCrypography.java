import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.util.Base64;

public class AshSymmetricCrypography {

    public AshSymmetricCrypography() {
    }

    public String encrypt(byte[] iv, String plaintext, SecretKey key) throws Exception {
        byte[] decrypted = plaintext.getBytes();
        byte[] encrypted = encrypt(iv, decrypted, key);

        StringBuilder ciphertext = new StringBuilder();

        ciphertext.append(Base64.getEncoder().encode(iv));
        ciphertext.append(":");
        ciphertext.append(Base64.getEncoder().encode(encrypted));

        return ciphertext.toString();
    }

    public byte[] encrypt(byte[] iv, byte[] plaintext, SecretKey key) throws Exception {
        Cipher cipher = Cipher.getInstance(key.getAlgorithm() + "/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        return cipher.doFinal(plaintext);
    }

    public String decrypt(String ciphertext, SecretKey key) throws Exception {
        String[] parts = ciphertext.split(":");
        byte[] iv = Base64.getDecoder().decode(parts[0]);
        byte[] encrypted = Base64.getDecoder().decode(parts[1]);
        byte[] decrypted = decrypt(iv, encrypted, key);
        return new String(decrypted);
    }

    public byte[] decrypt(byte[] iv, byte[] ciphertext, SecretKey key) throws Exception {
        Cipher cipher = Cipher.getInstance(key.getAlgorithm() + "/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        return cipher.doFinal(ciphertext);
    }
}
