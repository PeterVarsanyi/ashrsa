import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.SecureRandom;

/**
 * A <code>generateKey()</code> method protected, felülírható az ősben, ha nem ilyen típusú kulcsot szeretnénk.
 * A kulcsgenerálás alapértelmezetten: AES.
 *
 * @author Varsányi Péter
 */
public class AshSymmetricKeyGenerator {

    /**
     * A kulcs maga.
     */
    private SecretKey key;

    /**
     * Az osztály példányisítása.
     * Létrehoz egy új kulcsot.
     * A kulcsgenerálás alapértelmezetten: AES.
     *
     * @throws Exception Kulcs generálása közbeni hiba.
     */
    public AshSymmetricKeyGenerator() throws Exception {
        generateKey();
    }

    /**
     * A kulcs.
     *
     * @return A kulcs.
     */
    public SecretKey getKey() {
        return key;
    }

    /**
     * A kulcs megadása.
     *
     * @param key A kulcs.
     */
    public void setKey(SecretKey key) {
        this.key = key;
    }

    /**
     * 16 byte hosszú IV.
     *
     * @return
     */
    public byte[] generateIV() {
        SecureRandom random = new SecureRandom();
        byte[] iv = new byte[16];
        random.nextBytes(iv);
        return iv;
    }

    /**
     * A kulcs generálasa.
     * Initkor hívódik.
     * Protected, felülírható az ősben, ha nem ilyen típusú kulcsot szeretnénk.
     * A kulcsgenerálás alapértelmezetten: AES.
     *
     * @throws Exception Kulcs generálása közbeni hiba.
     */
    protected void generateKey() throws Exception {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        key = generator.generateKey();
    }
}
