import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.*;
import java.util.Base64;

public class TestAshAssymetricCryptography extends TestAshAssymetricKeyGenerator {

    private AshAssymetricCryptography crypt;

    @BeforeClass
    public void beginTests() throws NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException {
        super.beginTests();
        crypt = new AshAssymetricCryptography();
    }

    @AfterClass
    public void endTests() {
        super.endTests();
    }

    /**
     * Kulcs generálasa.
     * Irása file-ba, majd visszaolvasás és ellenőrzés, h a generált és a mentett egyezik-e?!
     *
     * @throws Exception Bármi hiba volt, akkor exception.
     */
    @Test
    public void testGetPrivate() throws Exception {
        gen.createKeys();
        gen.writeToFile(outputDir + privateKeyFileName, gen.getPrivateKey().getEncoded());
        File file2 = new File(outputDir + privateKeyFileName);
        Assert.assertTrue(file2.exists(), "Nem jött létre a privát kulcs file: " + outputDir + privateKeyFileName);
        PrivateKey pkey = crypt.getPrivate(outputDir + privateKeyFileName);
        Assert.assertNotNull(pkey, "Nem olvasott be privát kulcsot!");
        Assert.assertEquals(pkey, gen.getPrivateKey(), "Nem egyforma a két (generált és beolvasott) kulcs!");
    }

    /**
     * Kulcs generálasa.
     * Irása file-ba, majd visszaolvasás és ellenőrzés, h a generált és a mentett egyezik-e?!
     *
     * @throws Exception Bármi hiba volt, akkor exception.
     */
    @Test
    public void testGetPublic() throws Exception {
        gen.createKeys();
        gen.writeToFile(outputDir + publicKeyFileName, gen.getPublicKey().getEncoded());
        File file2 = new File(outputDir + publicKeyFileName);
        Assert.assertTrue(file2.exists(), "Nem jött létre a public kulcs file: " + outputDir + publicKeyFileName);
        PublicKey pkey = crypt.getPublic(outputDir + publicKeyFileName);
        Assert.assertNotNull(pkey, "Nem olvasott be public kulcsot!");
        Assert.assertEquals(pkey, gen.getPublicKey(), "Nem egyforma a két (generált és beolvasott) kulcs!");
    }

    /**
     * A blokkosított (<code>AshAssymetricCryptography.cryptData</code>) olvasó tesztelése a <code>encryptText</code> methodon keresztül.
     *
     * @throws GeneralSecurityException
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testCryptData() throws GeneralSecurityException, UnsupportedEncodingException {
        gen.createKeys();
        String plainMessage = "árvíztűrőtüköfúrógép 1234567890 !@#$%^&*(),. ÁRVÍTŰRŐTÜKÖRFÚRÓGÉP --<>";
        String codedMsg = crypt.encryptText(plainMessage, gen.getPrivateKey());
        String d2 = Base64.getEncoder().encodeToString(crypt.getCipher().doFinal(plainMessage.getBytes("UTF-8")));
        Assert.assertEquals(d2, codedMsg, "Nem egyezik meg a két kódolás eredménye! Hibás a cryptData method!");
    }

    /**
     * Szöveges üzenet titkosítása és feloldása teszt.
     *
     * @throws GeneralSecurityException     Bármi hiba volt, akkor exception.
     * @throws UnsupportedEncodingException Bármi hiba volt, akkor exception.
     */
    @Test
    public void testTextDeEncrypt() throws GeneralSecurityException, UnsupportedEncodingException {
        gen.createKeys();
        String plainMessage = "árvíztűrőtüköfúrógép 1234567890 !@#$%^&*(),. ÁRVÍTŰRŐTÜKÖRFÚRÓGÉP --<>";
        String codedMsg = crypt.encryptText(plainMessage, gen.getPrivateKey());
        Assert.assertNotNull(codedMsg, "Nem adot vissza kódolt szöveget az eljárás!");
        Assert.assertNotEquals(plainMessage, codedMsg, "Nem kódolta a szöveget az eljárás!");
        String encodedMsg = crypt.decryptText(codedMsg, gen.getPublicKey());
        Assert.assertEquals(encodedMsg, plainMessage, "A feloldás nem a megfelelő eredményt adta vissza!");
    }

    /**
     * Szöveges üzenet titkosítása filba.
     *
     * @throws GeneralSecurityException     Bármi hiba volt, akkor exception.
     * @throws UnsupportedEncodingException Bármi hiba volt, akkor exception.
     */
    @Test
    public void testTextDeEncryptFile() throws GeneralSecurityException, IOException {
        gen.createKeys();
        String plainMessage = "árvíztűrőtüköfúrógép 1234567890 !@#$%^&*(),. ÁRVÍTŰRŐTÜKÖRFÚRÓGÉP --<>";
        File ofile = new File(outputDir + "msg_decoded.txt");
        crypt.encryptToFile(plainMessage.getBytes("UTF-8"), ofile, gen.getPrivateKey());
        Assert.assertTrue(ofile.exists(), "Nem jött létre a titkosított szöveg file: " + ofile.toPath());

        Assert.assertNotEquals(Files.readAllBytes(ofile.toPath()), plainMessage.getBytes("UTF-8"), "Egyforma a titkosított és a titkosítatlan tartalom!");
        ofile.delete();
    }

    /**
     * Megadott kép kódolása majd dekódolása.
     * Meg kell adni a titkosítandó kép könyvtárát és a kép nevét.
     * Ugyanabba könyvtárba teszti a kódolt - "DECODED_"+kép file neve - állományt.
     * Majd felolvassa a kódoltat és dekódolja ugyanabba a könyvtárba - "ENCODED_" + kép file neve.
     *
     * @throws IOException              Bármi hiba volt, akkor exception.
     * @throws GeneralSecurityException Bármi hiba volt, akkor exception.
     */
    @Test
    public void testPictureDeEncryptFile() throws IOException, GeneralSecurityException {
        String path = "D:\\Temp\\";
        String fileName = "teszt_pic.jpg";

        File sourceFile = new File(path + fileName);
        Assert.assertTrue(sourceFile.exists(), "Nem létezik a megadott kép file: " + path + fileName);

        gen.createKeys();

        // file kódolása
        File decodedFile = new File(path + "DECODED_" + fileName);
        byte[] imageContent = getFileInBytes(sourceFile);
        crypt.encryptToFile(imageContent, decodedFile, gen.getPrivateKey());
        Assert.assertTrue(decodedFile.exists(), "Nem létezik a titkosított file: " + decodedFile.toPath());

        // file dekódolása majd mentése
        File encodedFile = new File(path + "ENCODED_" + fileName);
        byte[] decodedContent = getFileInBytes(decodedFile);
        crypt.decryptToFile(decodedContent, encodedFile, gen.getPublicKey());
        Assert.assertTrue(encodedFile.exists(), "Nem létezik a feloldott file: " + encodedFile.toPath());
    }

    private byte[] getFileInBytes( File f ) throws IOException {
        FileInputStream fis = new FileInputStream(f);
        byte[] fbytes = new byte[(int) f.length()];
        fis.read(fbytes);
        fis.close();
        return fbytes;
    }
}
