import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * A kulcsgenerátor tesztelése.
 */
public class TestAshAssymetricKeyGenerator {

    protected AshAssymetricKeyGenerator gen;
    protected String outputDir;
    protected String publicKeyFileName;
    protected String privateKeyFileName;

    /**
     * Lokális változó feltöltése.
     * A kulcsgenerátor objektum példányosítása és a file nevek, elérési út megadása.
     *
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     */
    @BeforeClass
    public void beginTests() throws NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException {
        gen = new AshAssymetricKeyGenerator(AshAssymetricKeyGenerator.KEYSIZE.KEY1024);
        outputDir = "d:/Temp/";
        publicKeyFileName = "ashTestPubKey.pub";
        privateKeyFileName = "ashTestPrivKey";
    }

    @AfterClass
    public void endTests() {
        File file1 = new File(outputDir + publicKeyFileName);
        if ( file1.exists() ) {
            file1.delete();
        }
        File file2 = new File(outputDir + privateKeyFileName);
        if ( file2.exists() ) {
            file2.delete();
        }
    }

    @Test
    public void testCreateKeys() throws Exception {
        gen.createKeys();
        Assert.assertTrue(gen.getPrivateKey() != null, "Nincs privát kulcs!");
        Assert.assertTrue(gen.getPublicKey() != null, "Nincs publikus kulcs!");
    }

    @Test
    public void testWriteToFile() throws Exception {
        gen.createKeys();
        Assert.assertTrue(gen.getPrivateKey() != null, "Nincs privát kulcs!");
        Assert.assertTrue(gen.getPublicKey() != null, "Nincs publikus kulcs!");

        gen.writeToFile(outputDir + publicKeyFileName, gen.getPublicKey().getEncoded());
        File file1 = new File(outputDir + publicKeyFileName);
        Assert.assertTrue(file1.exists(), "Nem jött létre a publikus kulcs file: " + outputDir + publicKeyFileName);

        gen.writeToFile(outputDir + privateKeyFileName, gen.getPrivateKey().getEncoded());
        File file2 = new File(outputDir + privateKeyFileName);
        Assert.assertTrue(file2.exists(), "Nem jött létre a privát kulcs file: " + outputDir + privateKeyFileName);
    }

}