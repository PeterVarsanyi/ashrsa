import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * Az aláirás ellenőrzése.
 */
public class TestAshSigning {

    protected AshAssymetricKeyGenerator gen;
    protected String outputDir;

    /**
     * Lokális változó feltöltése.
     * A kulcsgenerátor objektum példányosítása és a file nevek, elérési út megadása.
     *
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     */
    @BeforeClass
    public void beginTests() throws NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException {
        gen = new AshAssymetricKeyGenerator(AshAssymetricKeyGenerator.KEYSIZE.KEY1024);
        outputDir = "/home/peter/Temp/";
    }

    @AfterClass
    public void endTests() {
    }

    @Test
    public void testSigning() throws Exception {
        // kulcsok
        gen.createKeys();

        //adat
        byte[] data = "test".getBytes("UTF8");

        // aláírás képzése
        AshSigning signing = new AshSigning();
        byte[] signaure = signing.createSignature(data, gen.getPrivateKey());
        Assert.assertTrue(signaure != null, "Nincs aláírás!");

        // ellenőrzés
        Boolean checked = signing.verify(data, signaure, gen.getPublicKey());
        Assert.assertTrue(checked, "Nem stimmel az aláírás! Ellenőrzés false!");
    }

    @Test
    public void testSigningFile() throws Exception {
        // alírandó adattartalom file-ból
        byte[] data = Files.readAllBytes(new File("/home/peter/IdeaProjects/ashrsa/ProjectFiles/picture.jpg").toPath());

        // kulcsok képzése
        gen.createKeys();

        // aláírás
        AshSigning signing = new AshSigning();
        byte[] signaure = signing.createSignature(data, gen.getPrivateKey());
        Assert.assertTrue(signaure != null, "Nincs aláírás!");

        // ellenőrzés
        Boolean checked = signing.verify(data, signaure, gen.getPublicKey());
        Assert.assertTrue(checked, "Nem stimmel az aláírás! Ellenőrzés false!");
    }

}
